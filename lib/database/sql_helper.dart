import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart' as sql;
import 'package:sqflite_midterm_project/model/diary_model.dart';

class SQLHelper{
  SQLHelper._privateConstructor();
  static final SQLHelper useDatabase = SQLHelper._privateConstructor();

  static const _dbName = 'diary.db';

  static sql.Database? _database;
  Future<sql.Database> get database async => _database ??= await _initDatabase();

  Future<sql.Database> _initDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String getPath = join(directory.path, _dbName);

    return await sql.openDatabase(
      getPath,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(sql.Database db, int version) async {
    await db.execute('''
      CREATE TABLE diaryTable (
          id INTEGER PRIMARY KEY,
          title TEXT,
          date TEXT,
          description TEXT,
          image TEXT
      )
      ''');
  }

  Future<List<DiaryModel>> getDiaries() async {
    sql.Database db = await useDatabase.database;
    var diaries = await db.query('diaryTable', orderBy: 'id');
    List<DiaryModel> diaryList = diaries.isNotEmpty
        ? diaries.map((c) => DiaryModel.fromMap(c)).toList()
        : [];
    return diaryList;
  }

  Future<List<DiaryModel>> getDiary(int id) async{
    sql.Database db = await useDatabase.database;
    var diary = await db.query('diaryTable', where: 'id = ?', whereArgs: [id]);
    List<DiaryModel> diaryList = diary.isNotEmpty
        ? diary.map((c) => DiaryModel.fromMap(c)).toList()
        : [];
    return diaryList;
  }

  Future<int> add(DiaryModel diary) async {
    sql.Database db = await useDatabase.database;
    return await db.insert('diaryTable', diary.toMap(), conflictAlgorithm: sql.ConflictAlgorithm.replace,);
  }

  Future<int> remove(int id) async {
    sql.Database db = await useDatabase.database;
    return await db.delete('diaryTable', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> update(DiaryModel diary) async {
    sql.Database db = await useDatabase.database;
    return await db.update('diaryTable', diary.toMap(),
        where: "id = ?", whereArgs: [diary.id]);
  }
}
