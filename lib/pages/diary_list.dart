import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sqflite_midterm_project/database/sql_helper.dart';
import 'package:sqflite_midterm_project/model/diary_model.dart';
import 'package:sqflite_midterm_project/pages/add_diary.dart';
import 'package:sqflite_midterm_project/pages/edit_diary.dart';
import 'package:sqflite_midterm_project/pages/show_diary.dart';

class DiaryList extends StatefulWidget {
  const DiaryList({super.key});

  @override
  State<DiaryList> createState() => _DiaryListState();
}

class _DiaryListState extends State<DiaryList> {

  int? selectedId;

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Diary List'),
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: IconButton(
              icon: const Icon(Icons.add),
              tooltip: 'Add Diary',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const AddDiary()),
                );
              },
            ),
          ),
        ],
      ),
      body: Center(
        child: FutureBuilder<List<DiaryModel>>(
          future: SQLHelper.useDatabase.getDiaries(),
          builder: (BuildContext context, AsyncSnapshot<List<DiaryModel>> snapshot) {
            if (!snapshot.hasData) {
              return const CircularProgressIndicator(
                backgroundColor: Colors.tealAccent,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.amberAccent),
                strokeWidth: 5,
              );
            }
            return snapshot.data!.isEmpty
                ? const Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image(image: AssetImage('assets/diary.png'), height: 150),
                        SizedBox(height: 20),
                        Text('No Diary in list.', style: TextStyle(fontSize: 25, fontWeight: FontWeight.w500)),
                        Text('Add your new diary.', style: TextStyle(fontSize: 15, fontWeight: FontWeight.normal)),
                      ],
                    ),
                  )
                : ListView(
              children: snapshot.data!.map((diary) {
                return Center(
                  child: Card(
                    margin: const EdgeInsets.all(12),
                    color: selectedId == diary.id
                        ? Colors.amber
                        : Colors.amberAccent,
                    clipBehavior: Clip.hardEdge,
                    child: InkWell(
                      splashColor: Colors.limeAccent.withAlpha(30),
                      onTap: () {
                        debugPrint('Card tapped.');
                      },
                      child: Column(
                        children: [
                          Image.file(
                            File(diary.image),
                            fit: BoxFit.cover,
                            width: double.maxFinite,
                          ),
                          ListTile(
                            title: Text(diary.title),
                            titleTextStyle: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black),
                            subtitle: Text(diary.date),
                            subtitleTextStyle: const TextStyle(fontSize: 15, color: Colors.black),
                            onTap: () {
                              setState(() {
                                debugPrint(diary.image);
                                if (selectedId == null) {
                                  //firstname.text = grocery.firstname;
                                  selectedId = diary.id;
                                } else {
                                  // textController.text = '';
                                  selectedId = null;
                                }
                              });
                              var showDiary = diary.id;
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => ShowDiary(id: showDiary)
                              ));
                            },
                            trailing: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                IconButton(
                                  padding: const EdgeInsets.all(0),
                                  icon: const Icon(Icons.edit, color: Colors.teal),
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => EditDiary(diary)),
                                    );
                                  },
                                ),
                                IconButton(
                                  padding: const EdgeInsets.all(0),
                                  icon: const Icon(Icons.delete, color: Colors.teal),
                                  onPressed: () {
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title: const Column(
                                            children: [
                                              Text("Delete Diary", style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
                                              Text("Are you want to delete this diary?", style: TextStyle(fontSize: 15, fontWeight: FontWeight.normal)),
                                            ],
                                          ),
                                          actions: [
                                            Visibility(
                                              visible: true,
                                              child: TextButton(
                                                onPressed: () {
                                                  Navigator.of(context).pop();
                                                },
                                                child: const Text("No"),
                                              ),
                                            ),
                                            TextButton(
                                              onPressed: () {
                                                SQLHelper.useDatabase.remove(diary.id!);
                                                setState(() {
                                                  Navigator.of(context).pop();
                                                });
                                              },
                                              child: const Text("Yes"),
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }).toList(),
            );
          },
        ),
      ),
    );
  }
}
