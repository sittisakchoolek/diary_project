import 'package:flutter/material.dart';
import 'package:sqflite_midterm_project/pages/about_us.dart';
import 'package:sqflite_midterm_project/pages/add_diary.dart';
import 'package:sqflite_midterm_project/pages/diary_list.dart';

void main() {
  runApp(const MyApp());
  WidgetsFlutterBinding.ensureInitialized();
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.greenAccent,
          brightness: Brightness.dark,
        ),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePage();
}

class _MyHomePage extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
        centerTitle: true,
      ),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.green,
              ),
              margin: EdgeInsets.zero,
              padding: EdgeInsets.zero,
              child: Image(
                image: AssetImage('assets/diary_background.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            drawerListTile(context, 'Home', const MyApp()),
            const Divider(color: Colors.white),
            drawerListTile(context, 'Diary List', const DiaryList()),
            const Divider(color: Colors.white),
            drawerListTile(context, 'About Us', const AboutUs()),
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Image(image: AssetImage('assets/home.png'), height: 150),
              const SizedBox(height: 20),
              const Text('Welcome to home page!', style: TextStyle(fontSize: 25, fontWeight: FontWeight.w500)),
              const Text('Add your new diary every day.', style: TextStyle(fontSize: 15, fontWeight: FontWeight.normal)),
              const SizedBox(height: 35),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    child: const Text('My Diary'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const DiaryList()),
                      );
                    },
                  ),
                  const SizedBox(width: 35),
                  ElevatedButton(
                    child: const Text('Add Diary'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const AddDiary()),
                      );
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  ListTile drawerListTile(BuildContext context, String text, Widget page) {
    return ListTile(
      title: Text('    $text'),
      titleTextStyle: const TextStyle(fontSize: 20, fontWeight: FontWeight.w300),
      onTap: () {
        Navigator.pop(context); // Close the drawer
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => page));
      },
    );
  }
}